# Low rank matrix denoising for count data with unbiased KL risk estimation

This software is concerned by the analysis of observations organized
in a matrix form whose elements are count data assumed to follow a
Poisson or a multinomial distribution. We focus on the estimation of
either the intensity matrix (Poisson case) or the compositional matrix
(multinomial case) that is assumed to have a low rank structure. We
propose to construct an estimator minimizing the regularized negative
log-likelihood by a nuclear norm penalty. Our approach easily yields a
low-rank matrix-valued estimator with positive entries which belongs
to the set of row-stochastic matrices in the multinomial case. Then,
our main contribution is to propose a data-driven way to select the
regularization parameter in the construction of such estimators by
minimizing (approximately) unbiased estimates of the Kullback-Leibler
(KL) risk in such models. The evaluation of these quantities is a
delicate problem, and we introduce novel methods to obtain accurate
numerical approximation of such unbiased estimates. Simulated data are
used to validate this way of selecting regularizing parameters for
low-rank matrix estimation from count data. Examples from a survey
study and metagenomics also illustrate the benefits of our approach
for real data analysis.

## License

This software is governed under th CeCILL license.
See files LICENSE / LICENCE for more details.

## Description / Installation

This is a Python 3 software. Demo files are Jupyter notebooks that
reproduces some of the figures in:

> Jérémie Bigot, Charles Deledalle. Low rank matrix denoising for
> count data with unbiased Kullback-Leibler risk estimation.
> Technical paper. 2020

