import numpy as np
from functools import reduce
import scipy.linalg as scl
import finitediff_minus_one as fmo


def omega(Z):
    m = Z.max(axis=1, keepdims=True)
    e = np.exp(Z - m)
    return e / e.sum(axis=1, keepdims=True)


def omega_inv(P):
    l = np.log(P)
    return l - l.mean(axis=1, keepdims=True)


# Toy estimator
def shrink_naive(Y, w=.5, eps=1.5):
    m = Y.shape[0]
    k = Y.shape[1]
    Y_plus = np.maximum(Y, 0)
    ni_plus = Y_plus.sum(axis=1, keepdims=True)
    mY_plus = Y_plus.mean(axis=1, keepdims=True)
    P_hat = 1. / k + w * (Y_plus - mY_plus) / (eps + ni_plus)
    return P_hat


# Toy estimator minus 1
def shrink_naive_m1(Y, w=.5, eps=1.5):
    m = Y.shape[0]
    k = Y.shape[1]
    Y_plus_m1 = np.maximum(Y - 1, 0)
    ni_bis = Y.sum(axis=1, keepdims=True) + (Y_plus_m1 - Y)
    mY_bis = Y.mean(axis=1, keepdims=True) + (Y_plus_m1 - Y) / k
    Phat = 1. / k + w * (Y_plus_m1 - mY_bis) / (eps + ni_bis)


def shrink_naive(y, w=.45, eps=1.5):
    m = y.shape[0]
    k = y.shape[1]
    y = np.maximum(y, 0)
    ni = y.sum(axis=1, keepdims=True)
    my = y.mean(axis=1, keepdims=True)
    Phat = 1. / k + w * (y - my) / (eps + ni)
    return Phat


# Toy estimator minus 1
def shrink_naive_m1(y, w=.45, eps=1.5):
    m = y.shape[0]
    k = y.shape[1]
    ym1 = np.maximum(y-1, 0)
    ni = y.sum(axis=1, keepdims=True) + (ym1 - y)
    my = y.mean(axis=1, keepdims=True) + (ym1 - y) / k
    Phat = 1. / k + w * (ym1 - my) / (eps + ni)
    return Phat


# Variational model
def shrink(Y, lbd=1, T=100, centering='both'):
    m = Y.shape[0]
    k = Y.shape[1]
    ni = Y.sum(axis=1, keepdims=True)

    P_MLplus = shrink_naive(Y)

    # Rule of thumb: lambda should be approximatively proportional
    # to the standard deviation of (Y / ni) which is 1 / sqrt(ni)
    lbd = lbd / np.sqrt(ni.mean())

    t = 1
    gamma = 2
    Z = omega_inv(P_MLplus)
    for i in range(T):
        Z_old = Z.copy()

        grad_F_Z = omega(Z) - Y / ni
        Z = Z - gamma * grad_F_Z
        if lbd > 0:
            if centering is 'row':
                mean_Z = Z.mean(axis=0, keepdims=True)
            if centering is 'col':
                mean_Z = Z.mean(axis=1, keepdims=True)
            if centering is 'both':
                mean_Z = \
                    Z.mean(axis=0, keepdims=True) + \
                    Z.mean(axis=1, keepdims=True) - \
                    Z.mean()
            if centering is None:
                mean_Z = 0
            Z = Z - mean_Z
            U, S, VH = np.linalg.svd(Z, full_matrices=False)
            S = np.maximum(S - gamma * lbd, 0)
            Z = np.dot(U * S, VH)
            Z = mean_Z + Z

        # FISTA update
        t_old = t
        t = (1 + np.sqrt(1 + 4 * t**2)) / 2.
        Z = Z + (t_old - 1) / t * (Z - Z_old)

    P_hat = omega(Z)
    return P_hat


def KL(P, P_hat_func, Y):
    P_hat = P_hat_func(Y)
    return -(P * np.log(P_hat)).sum()


def UKL(P_hat_func, Y, eps=.1, directions=None, order=1):
    if not directions:
        directions = []
        for o in range(order):
            directions = directions + \
                [(2 * np.random.binomial(1, .5, size=y.shape) - 1)]
    ni = Y.sum(axis=1, keepdims=True)

    log_P_hat_m1 = fmo.findiff_m1_expansion(lambda Y: np.log(P_hat_func(Y)),
                                            Y, eps, directions[:order+1],
                                            order=order+1)
    return (-1 / ni * Y * log_P_hat_m1).sum()


def UKL_naive(Y, w):
    ni = Y.sum(axis=1, keepdims=True)
    log_P_hat_m1 = np.log(shrink_naive_m1(Y, w))
    return (-1 / ni * Y * log_P_hat_m1).sum()


def UKL_naive_false(Y, w):
    ni = Y.sum(axis=1, keepdims=True)
    log_P_hat_m1 = np.log(shrink_naive_m1(Y, w))
    return (-1 / (ni+1) * Y * log_P_hat_m1).sum()
